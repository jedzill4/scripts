#!/bin/env python3

import sys,os
import argparse as arg
import numpy as np

PROG = 'raw2mean'

def main():
    argparser = arg.ArgumentParser(description="Toma como argumento una carpea que contenga datos y calcula la media de todas las simulaciones", prog=PROG)
    argparser.add_argument('directorio',help="Directorio de entrada")
    argparser.add_argument('--npz',action='store_true',help="Guardar binario con el contenido de la media comprimido (gz)")
    argparser.add_argument('--npz-nc',action='store_true',help="Guardar binario con el contenido de la media NO comprimido")
    argparser.add_argument('--outfile','-o',action='store_true',help="Guardar archivo de datos")

    args = argparser.parse_args()
    if not (args.outfile or args.npz or args.npz_nc):
        sys.stderr.write("error: debe tener elegir al menos una forma de salida\n")
        sys.stderr.flush()
        sys.exit(1)

    DIR = args.directorio

    TOTSIM = []
    for sim in os.listdir(DIR):
        DAT = np.loadtxt(DIR+"/"+sim, unpack=True)
        TOTSIM.append(DAT)

    TOTSIM  = np.array(TOTSIM)
    MEANSIM = np.mean(TOTSIM,axis=0)

    if args.npz:
        np.savez(DIR+".npz",MEANSIM)
    if args.npz_nc:
        np.savez(DIR+".nc.npz",MEANSIM)
    if args.outfile:
        np.savetxt(DIR+".mean.dat.gz",MEANSIM,fmt='%.4f', delimiter =' ')
    return 0

if __name__ == "__main__":
    sys.exit(main())
