#!/bin/env python3

import sys,os
import argparse as arg
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import matplotlib.gridspec as gridspec

FILENAME  = 'out_aedes2008.npz'

#fig,axes = plt.subplots(nrows=2,ncols=3)#, sharex='all', sharey='all')
fig = plt.figure()
gs = gridspec.GridSpec(3,3) 
axes = np.array([[plt.subplot(gs[0,0]),
                plt.subplot(gs[0,1]),
                plt.subplot(gs[0,2])],
               [plt.subplot(gs[1,0]),
                plt.subplot(gs[1,1]),
                plt.subplot(gs[1,2])]
              ])
ax_temp = plt.subplot(gs[2,:])
I = 3   # Numero de columnas
J = 3   # Numero de filas
N = 6   # Numero de poblaciones

npzfile = np.load(FILENAME)
DATA = npzfile['arr_0']
TIME = DATA[0]
TEMP = DATA[1]

gPOP = []
for n in range(N):
    POP = []
    for i in range(I):
        col = []
        for j in range(J):
            col.append(DATA[2+n +i*J+j])
        col = np.array(col)
        POP.append(col)
    POP = np.array(POP)
    gPOP.append(POP)
del POP
gPOP = np.array(gPOP)

print(gPOP.shape)
print(gPOP[0,:,:,0])

def init():
    N1=axes[0,0].imshow(gPOP[0,:,:,0],cmap=plt.cm.gist_yarg, interpolation='none')
    N2=axes[0,1].imshow(gPOP[1,:,:,0],cmap=plt.cm.gist_yarg, interpolation='none')
    N3=axes[0,2].imshow(gPOP[2,:,:,0],cmap=plt.cm.gist_yarg, interpolation='none')
    N4=axes[1,0].imshow(gPOP[3,:,:,0],cmap=plt.cm.gist_yarg, interpolation='none')
    N5=axes[1,1].imshow(gPOP[4,:,:,0],cmap=plt.cm.gist_yarg, interpolation='none')
    N6=axes[1,2].imshow(gPOP[5,:,:,0],cmap=plt.cm.gist_yarg, interpolation='none')
    temp, = ax_temp.plot(TIME,TEMP,'r-',lw=2)
    return [N1,N2,N3,N4,N5,N6,temp]
def animate(i):
    N1=axes[0,0].imshow(gPOP[0,:,:,i],cmap=plt.cm.gist_yarg, interpolation='none')
    N2=axes[0,1].imshow(gPOP[1,:,:,i],cmap=plt.cm.gist_yarg, interpolation='none')
    N3=axes[0,2].imshow(gPOP[2,:,:,i],cmap=plt.cm.gist_yarg, interpolation='none')
    N4=axes[1,0].imshow(gPOP[3,:,:,i],cmap=plt.cm.gist_yarg, interpolation='none')
    N5=axes[1,1].imshow(gPOP[4,:,:,i],cmap=plt.cm.gist_yarg, interpolation='none')
    N6=axes[1,2].imshow(gPOP[5,:,:,i],cmap=plt.cm.gist_yarg, interpolation='none')
    temp_back, = ax_temp.plot(TIME,TEMP,'r-',lw=2)
    temp = ax_temp.axvline(x=i+365,lw=3,color='k')
    return [N1,N2,N3,N4,N5,N6,temp_back,temp]

Frames = gPOP.shape[-1]
ani = animation.FuncAnimation(fig, animate, init_func=init,
                                       frames=Frames, interval=20, blit=True)

titles = iter(['Huevos','Larvas','Pupas','Adultos 1','Voladoras','Adultos 2'])
for axarray in axes:
    for ax in axarray:
        ax.set_title(next(titles))
        # Move left and bottom spines outward by 10 points
        ax.spines['left'].set_position(('outward', 10))
        ax.spines['bottom'].set_position(('outward', 10))
        # Hide the right and top spines
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)

        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')

ax_temp.set_xlabel("tiempo [dias]")
ax_temp.set_ylabel("temperatura [C]")
ax_temp.set_xlim(TIME[0],TIME[-1])

plt.tight_layout()

#ani.save('aedes2008_grid.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
plt.show()
